# FairCoin.Co electrumfair daemon

electrumfair daemon for FairCoin.Co

The service is made for/controlled by gitlab runner.

## Usage

Go Gitlab **CI/CD** -> **Pipeline** and **Run Pipeline**

Enter variable name **CMD**

#### CMD - commands

~~~
build        # build container ( changes of Dockerfile )
start        # start container ( changes of scripts )
stop         # stop container
uninstall    # remove container without to delete the data
remove       # remove all data
~~~


#### CI/CD Settings

Go Gitlab **Settings** -> **CI/CD** -> **Variables**

~~~
#### FairCoin.Co group variables ######################
ELECTRUMFAIR_CONF        # conf
~~~

## Development <small>( manual usage )</small>

If you want create an instance manual then follow the  instructions.

1. install docker and docker-compose ( https://docs.docker.com/compose/install/ )
1. clone this project
1. change configuration in ./env
1. run services by ./control.sh

~~~
chmod +x ./control.sh
./control.sh build
./control.sh start
./control.sh stop
./control.sh uninstall
./control.sh remove
~~~
