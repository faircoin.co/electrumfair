#!/bin/bash
export RUNNER_UID=`id -u`
export RUNNER_GID=`id -g`
export `cat ./env/*`

case $1 in
  "build")
      mkdir -p ~/.electrumfair
      docker-compose stop
      docker-compose down
      docker-compose build
    ;;
  "start")
      source ${ELECTRUMFAIR_CONF}
      docker-compose stop
      docker-compose down
      docker-compose up -d
      sleep 10
      docker-compose exec -T electrumfair ./run_electrumfair setconfig rpchost ${rpcconnect}
      docker-compose exec -T electrumfair ./run_electrumfair setconfig rpcport ${rpcport}
      docker-compose exec -T electrumfair ./run_electrumfair setconfig rpcuser ${rpcuser}
      docker-compose exec -T electrumfair ./run_electrumfair setconfig rpcpassword ${rpcpassword}
      docker-compose exec -T electrumfair ./run_electrumfair setconfig server ${server}
      docker-compose stop
      docker-compose up -d
    ;;
  "stop")
      docker-compose stop
    ;;
  "uninstall")
      docker-compose stop
      docker-compose down
    ;;
  "remove")
      docker-compose stop
      docker-compose down
#      rm -R ~/.electrumfair  #### dont use this command if you have some wallet.dat in your ~/.faircoin2 folder !!! In this case remove the block databases manually.
    ;;
esac
