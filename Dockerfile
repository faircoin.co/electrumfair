# Pull base image.
FROM ubuntu:18.04@sha256:5f4bdc3467537cbbe563e80db2c3ec95d548a9145d64453b06939c4592d67b6d

ENV LC_ALL=C.UTF-8 LANG=C.UTF-8

#####################################################################################root##
### install base libraries and dependencies for faircoin daemon ###########################
RUN apt-get update -q && \
    apt-get install -qy \
        git \
        python3-pyqt5

#####################################################################################root##
### install nano commandline editor [optional] ( to edit faircoin.conf later if necessary )
RUN apt-get install -qy nano

#####################################################################################root##
### install python packages
RUN apt-get install -qy python3-pip
# RUN python3 -m pip install python-bitcoinrpc

#####################################################################################root##
### system cleanup ########################################################################
RUN rm -rf /var/lib/apt/lists/* && \
    apt-get autoremove -y && \
    apt-get clean

#####################################################################################root##
### create and run user account to image ##################################################
ARG RUNNER_GID
ARG RUNNER_UID
RUN groupadd -g $RUNNER_GID faircoin
RUN useradd --create-home --shell /bin/bash faircoin --uid $RUNNER_UID --gid $RUNNER_GID

USER faircoin
#################################################################################faircoin##
### download electrumfair daemon ##########################################################
WORKDIR /home/faircoin
RUN git clone https://github.com/faircoin/electrumfair.git

#################################################################################faircoin##
### initialize and setup electrumfair daemon ##############################################
USER root
RUN mkdir -p /home/faircoin/.electrumfair
RUN chown -R faircoin:faircoin /home/faircoin/.electrumfair


USER faircoin
WORKDIR /home/faircoin/electrumfair
RUN python3 -m pip install --system -r contrib/deterministic-build/requirements.txt -t contrib/../packages
ENTRYPOINT ["./run_electrumfair"]
